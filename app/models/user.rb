# :id
# :email
# :role
class User < ApplicationRecord
  enum role: {admin: 0, user: 1}

  def admin?
    self.role == 'admin'
  end

  def make_admin
    self.role = 'admin'
    AdminLogger.new("#{self.email} was made admin").record
  end
end
