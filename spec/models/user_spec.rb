require 'rails_helper'

RSpec.describe User, type: :model do
  let(:user) { create(:user) }

  describe '#admin?' do
    context 'when user is admin' do
      let(:user) { create(:user, :admin) }

      it 'returns true' do
        expect(user.admin?).to eq(true)
      end
    end

    context 'when user is not admin' do
      it 'returns false' do
        expect(user.admin?).to eq(false)
      end
    end
  end

  describe '#make_admin' do
    let(:msg) { "#{user.email} was made admin" }
    let(:logger) { instance_double(AdminLogger) }

    before do
      allow(AdminLogger).to receive(:new).and_return(logger)
      allow(logger).to receive(:record)
      user.make_admin
    end

    it 'calls AdminLogger' do
      expect(AdminLogger).to have_received(:new).with(msg)
      expect(logger).to have_received(:record)
    end

    it 'changes role to admin' do
      expect(user.role).to eq('admin')
    end
  end
end
